// SOftUni==================================================================
// Programming Basics with JavaScript - януари 2020 - Прости операции и пресмятания
//   ----------------------------------------1-------------------------------

// function solution(input) {

//     let usd = Number(input.shift())
//     let courseBGN = 1.79549
//     let bgn = usd * courseBGN

//     console.log(bgn.toFixed(2))
// }

// solution([
//     20
// ])

// ------------------------------------------2------------------------------

// function solution(input) {
//  let radians = Number(input.shift())

//  let degrees = radians * 180 / Math.PI

//  console.log(degrees.toFixed())
// }

// solution([
//     3.1416
// ])

// ------------------------3------------------------------

// function solution(input) {
//     let x1 = Number(input.shift())
//     let y1 = Number(input.shift())

//     let x2 = Number(input.shift())
//     let y2 = Number(input.shift())

//     let height = Math.abs(y1 - y2)
//     let width = Math.abs(x2 - x1)

//     let area = height * width
//     let parameter = 2 * area

// console.log(area.toFixed(2))
// console.log(parameter.toFixed(2))
// }
// solution([
// 60,
// 20,
// 10,
// 50
// ])

// --------------------------4------------------------------

// function solution(input){

// let N = Number(input.shift())
// let L = Number(input.shift())
// let W = Number(input.shift())

// let area = N * (1.20 + 2 * 0.30) * (0.65 + 2 * 0.30)
// let quad = N * ( L / 2 ) * ( L /2 )

// let coverUsd = 7
// let quadUsd = 9  

// let priceUsd = area * coverUsd + quad * quadUsd
// let priceBgn = priceUsd * 1.82 

// console.log(area)
// console.log(quad.toFixed(2))
// console.log(`${priceUsd.toFixed(2)} USD`)
// console.log(`${priceBgn.toFixed(2)} BGN`)

// }

// solution([ 10, 1.20, 0.65 ])


// ==================================================
// Programming Basics with JavaScript - януари 2020  Проверки - if - else
 
// -----------------------1--------------------------

// function solution(input){
// let bonnusScore = 0
// let currentScore = Number(input.shift())

// if(currentScore <= 100){
//     bonnusScore = bonnusScore + 5
// } else if (currentScore > 1000) {
//     bonnusScore = bonnusScore + (currentScore * 0.10)
// } else if (currentScore > 100 ) {
//     bonnusScore = bonnusScore + (currentScore * 0.20)
// }

// let reminder = (currentScore % 2)
// let isEven = reminder === 0
// let isTheLastDigitalIsFive = (currentScore % 10) === 5

// if(isEven) {
//     bonnusScore = bonnusScore + 1
// }   else if(isTheLastDigitalIsFive) {
//     bonnusScore = bonnusScore + 2
// }
// console.log(bonnusScore)
// console.log(currentScore + bonnusScore)
// }

// solution ([
//     120
// ])

// -------------------2---------------------

// function speedInfo(input) {
// let speed = Number(input.shift())

//  if (speed <= 10) {
//      console.log('slow')
//  } else if ( speed <= 50) {
//      console.log("average")
//  } else if (speed <= 150 ) {
//     console.log("fast")
//  } else if (speed <=1000) {
//      console.log('ultra fast')
//  } else {
//     console.log('extreamly fast')
//  }

//  console.log(speed)
//  }

// speedInfo ([ 2500 ])

// -----------------------3--------------------------

// function solution(input) {
// let number = input.shift()
// let inputType = input.shift()
// let outputType = input.shift()

// if (inputType === 'mm'){
//     number = number * 10
// } else if (inputType ==='m') {
//     number = number * 100
// } 
// let result = 0

// if (outputType === 'mm') {
//   result = number / 10
// } else if (outputType ==='m') {
//     result = number / 100
// } else {
//  result = number   
// }
// console.log(result.toFixed(3))
// }
// solution([ 150,'m', 'sm' ])

// -------------------------4----------------------------

// function solution(input) {
//     let hours = input.shift()
//     let minutes = input.shift() + 15

//     if (minutes >= 60){
//         hours = hours +1
//         minutes = minutes - 60
//     }

//     if (hours >= 24){
//         hours = hours - 24
//     }

//     if(minutes >= 10) {
//         console.log(`${hours}:${minutes}`)
//     } else {
//         console.log(`${hours}:0${minutes}`)
//     }

// }
// solution ([1 , 46])

// ----------------------------5---------------------------
// function solution(input){

// let filmBudget = input.shift()
// let people = input.shift()
// let priceCloth = input.shift()

// let decor = filmBudget + 0.10

// let amount = people * priceCloth 

// if (people > 150 ) {
//     amount = amount * 0.90
// }

// let totalExpenses = decor + amount 

// if(filmBudget >= totalExpenses) {
//     console.log("action")
//     console.log(`filming start with ${(filmBudget - totalExpenses)} leva left  `)
// } else {
//     console.log('not enough money')
//     console.log(`Need  ${(totalExpenses - filmBudget).toFixed(2)} leva more `)
 
// }
// solution ([15437.62 , 186, 57.99])
// }

// ===================================================================
// Programming Basics with JavaScript - януари 2020  По - сложни проверки - if - else

// function compiler() {
// let day = document.getElementById('days').value
// let text = ''

// switch (day) {
//     case '1': text = 'monday'
//         break
//     case '2': text = 'tuesday'
//         break
//     case '3': text = 'wednesday'
//         break
//     case '4': text = 'thursday'
//         break
//     case '5': text = 'friday'
//         break
//     case '6': text = 'saturday'
//         break
//     case '7': text = 'sunday'
//         break
//     default: text = 'wrong'
//         break;
// }
// document.getElementById('show').innerHTML = text
// }
// ----------------------------2--------------------------------
// function compiler() {
//     let animals = document.getElementById('animal').value
//     let text = ''

//     switch (animals) {
//         case 'dog' : text = 'mammal'
//         break
//         case 'snake' : text = 'reptile'
//         break
//         case 'cat' : text = 'unknow'
//         break
//         default: text = 'wrong animal'
//     }
//     document.getElementById('show').innerHTML = text
// }
// --------------------------------3------------------------------
// function compiler() {
//         let gender = document.getElementById('genders').value
//         let age = document.getElementById('ages').value
//         let text = ''
//         if (gender === 'f') {
//             if (age >= 16) {
//                 text = 'Ms.'
//             }
//             else {
//                 text = 'Miss'
//             }
//         }
//         else if (gender === 'm') {
//             if (age >= 16) {
//                 text = 'Mr.'
//             }
//             else {
//                 text = 'Master'
//             }
//         }
//         else {
//             text = 'No Gender Found 0.0!'
//         }
//         document.getElementById('show').innerHTML = text
//     }
// -----------------------4----------------------------------

// function compiler() {
//  let product = document.getElementById('products').value
//  let town = document.getElementById('towns').value
//  let quantity = document.getElementById('quantities').value
//  let text = ''
//  if(product === "coffee") {
//     if(town === 'sofia') {
//         totalsum = quantity * 0.50
//         text = totalsum 
//     } else if (town === 'plovdiv') {
//         totalsum = quantity * 0.40
//         text = totalsum 
//     } else if (town === 'varna') {
//         totalsum = quantity * 0.45
//         text = totalsum 
//     }

//  } else if (product === "water") {
//     if(town === 'sofia') {
//         totalsum = quantity * 0.80
//         text = totalsum
//     } else if (town === 'plovdiv') {
//         totalsum = quantity * 0.70
//         text = totalsum
//     } else if (town === 'varna') {
//         totalsum = quantity * 0.75
//         text = totalsum
//     } 
// }
//    if (product === 'beer') {
//     if(town === 'sofia') {
//         totalsum = quantity *1.20
//         text = totalsum
//     } else if (town === 'plovdiv') {
//         totalsum = quantity * 1.10
//         text = totalsum
//     } else if (town === 'varna') {
//         totalsum = quantity * 1.15
//         text = totalsum
//     }

//  } if (product === 'sweets') {
//     if(town === 'sofia') {
//         totalsum = quantity *1.45
//         text = totalsum
//     } else if (town === 'plovdiv') {
//         totalsum = quantity * 1.30
//         text = totalsum
//     } else if (town === 'varna') {
//         totalsum = quantity * 1.35
//         text = totalsum
//     }

//  } if (product === 'peanuts') {
//     if(town === 'sofia') {
//         totalsum = quantity *1.60
//         text = totalsum
//     } else if (town === 'plovdiv') {
//         totalsum = quantity * 1.50
//         text = totalsum
//     } else if (town === 'varna') {
//         totalsum = quantity * 1.55
//         text = totalsum
//     }
//  } 

// document.getElementById('show').innerHTML = text
// }

// function solution(input) {
// let  product = input.shift()
// let  town = input.shift()
// let  quantity = (input.shift())

// let totalsum = 0 

//  if(product = "coffee") {
//     if(town === 'sofia') {
//         totalsum = quantity * 0.50
//     } else if (town === plovdiv) {
//         totalsum = quantity * 0.40
//     } else {
//         totalsum = quantity * 0.45
//     }

//  } else if (product = "water") {
//     if(town === 'sofia') {
//         totalsum = quantity * 0.80
//     } else if (town === plovdiv) {
//         totalsum = quantity * 0.70
//     } else {
//         totalsum = quantity * 0.75
//     }

//  } else if (product = 'beer') {
//     if(town === 'sofia') {
//         totalsum = quantity *1.20
//     } else if (town === plovdiv) {
//         totalsum = quantity * 1.10
//     } else {
//         totalsum = quantity * 1.15
//     }

//  } else if (product = 'sweets') {
//     if(town === 'sofia') {
//         totalsum = quantity *1.45
//     } else if (town === plovdiv) {
//         totalsum = quantity * 1.30
//     } else {
//         totalsum = quantity * 1.35
//     }

//  } else if (product = 'peanuts') {
//     if(town === 'sofia') {
//         totalsum = quantity *1.60
//     } else if (town === plovdiv) {
//         totalsum = quantity * 1.50
//     } else {
//         totalsum = quantity * 1.55
//     }
//  } 
// console.log(totalsum)
// }
// solution (['coffee' , 'sofia', 2])
// -------------------5--------------------------

// function compiler(text) {
// let number = document.getElementById('numbers').value
// let text = ''

// if (number >= '-100' && number <= '100' && number !== '0') {
//     text = 'yes'
// } else {
//     text = 'no'
// }

// document.getElementById('show').innerHTML = text
// }
// --------------------6-------------------------

// function compiler(text) {
//     let product = document.getElementById('products').value
//     let text = ''

//     if( product === 'banana' || product === 'apple' || product === 'orange') {
//         text = 'fruit' 
//     } else if (product === 'tomato' ||  product === 'cucumber' ||  product === 'carrot' ) {
//         text = 'vegetable'
//     } else { 
//          text = 'unknow'
//     }

//   document.getElementById('show').innerHTML = text
// }
// ------------------------7----------------------

// function compiler() {
// let number = document.getElementById('numbers').value
// let text = ''

// if ( (number >= 100 && number <= 200) || number === 0) {
// text = 'valid'
// } else {
//     text = 'invalid'
// }
// document.getElementById('show').innerHTML = text
// }
// ---------------8--------------------------

// function compiler() {
// let product = document.getElementById('products').value
// let day = document.getElementById('days').value
// let quantity = document.getElementById('quantities').value
// let text = ''
// let totalMoney = ''

// if ( day === 'monday' || day === 'tuesday' || day === 'wednesday'|| day === 'thursday' || day === 'friday' ) {
//  switch(product){
//      case 'banana': totalMoney = quantity * 2.50
//      text = 'totalMoney'
//      break
//      case 'apple': totalMoney = quantity * 1.50
//      text = totalMoney 
//      break
//      case 'orange': totalMoney = quantity * 3.00
//      text = totalMoney  
//      break
//      default: text = 'error'
//  }

// } else if (day === 'saturday' || day === 'sunday') {
//     switch(product){
//         case 'banana': totalMoney = quantity * 4.50
//         text = totalMoney 
//         break
//         case 'apple': totalMoney = quantity * 2.50 
//         text = totalMoney
//         break
//         case 'orange': totalMoney = quantity * 5.00  
//         text = totalMoney
//         break
//         default: text = 'error'
//     }
// } else {
//     text = 'error' 
// }
// if(totalMoney !== '0') {
//     text = totalMoney
// }
// document.getElementById('show').innerHTML = text
// }

//------------------------9------------------------

// function compiler() {
 
// let city = document.getElementById('cities').value
// let quantity = document.getElementById('quantities').value

// if( city === 'sofia'){
//     if (quantity <= 500) {
//     total = quantity * 0.05
//  } else if ( quantity <= 1000) {
//      total = quantity * 0.07
//  } else if ( quantity <= 10000) {
//     total = quantity * 0.08
//  } else {
//     total = quantity * 0.12
//  }

// } else if (city === 'varna') {
//     if (quantity <= 500) {
//     total = quantity * 0.45
//  } else if ( quantity <= 1000) {
//      total = quantity * 0.75
//  } else if ( quantity <= 10000) {
//     total = quantity * 0.10
//  } else {
//     total = quantity * 0.13
//  }

// } else if (city === 'plovdiv') {

//     if (quantity <= 500) {
//     total = quantity * 0.55
//  } else if ( quantity <= 1000) {
//      total = quantity * 0.08
//  } else if ( quantity <= 10000) {
//     total = quantity * 0.12
//  } else {
//     total = quantity * 0.14
//  }
// }
// text = total.toFixed(2)
// document.getElementById('show').innerHTML = text
// }

// -------------------10---------------------------

// function compiler() {

// let day = document.getElementById('days').value
// let roomType = document.getElementById('rooms').value
// let feetback = document.getElementById('feetb').value

// let nights = day - 1

// if (roomType === 'room for one person') {
//     price = 18
// } else if (roomType === 'apartment') {
//     price = 25
//     if(nights < 10) {
//         price = price * 0.7
//     } else if (nights >=10 && nights <=15 ) {
//         price = price * 0.65
//     } else  {
//         price = price * 0.5
//     }

// } else if (roomType === 'president apartment') {
//     price = 35
//     if (nights < 10) {
//         price = price * 0.9
//     } else if (nights >=10 && nights <=15 ) {
//         price = price * 0.85 
//     } else {
//         price = price * 0.8
//     }
// }

// let totalPrice = price * nights
// if (feetback === 'positive') {
//     totalPrice = totalPrice * 1.25
// } else if (feetback === 'negative') {
//     totalPrice = totalPrice * 0.9
// }
// text = totalPrice.toFixed(2) 
// document.getElementById('show').innerHTML = text
// }
// ------------------------------TASK-------------------------------
// function add() {
//     let prices = document.getElementById('price').value
//     let names = document.getElementById('name').value 
    
//     listNode = document.getElementById('list')
//     liNode = document.createElement('li')
//     txtNode = document.createTextNode(`${names} - ${prices}`)

//     totalPrice = prices 

//     liNode.appendChild(txtNode)
//     listNode.appendChild(liNode)
    
//     text =  totalPrice

//     document.getElementById('show').innerHTML = text
// }
// --------------------------LOOP-------------------------

