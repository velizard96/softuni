// JavaScript Crash Course For Beginners
// FIRST JS CODE
// ------------------------------------------------------
// const name = 'John';
// const age = 30;
 
// console.log('My name is ' + name + ' and I am ' + age);

// console.log(`My name is ${name} and I am ${age}`);

// const hello = `My name is ${name} and I am ${age}`;

// console.log(hello)
// ---------------------------------------------------------------------
// ------------------------------------------
// arrays

//  let fruits = ['apples', 'orange', 'pears', ];

//  fruits[3] = 'grapes'

//  fruits.push('mangos')

//  fruits.unshift('strawberries')

// console.log(Array.isArray(fruits))

// console.log(fruits)

// -------------------------------------------
// object

// let person = {
//     firstName: 'Michel',
//     lastName: 'Kors',
//     age: 30,
//     hobbies: ['music', 'movies', 'sports'],
//      
//     }
// }
// person.email = 'michelkors@gmail.com'

// console.log(person.hobbies[2])
// console.log(person.address.city)

// let {firstName, lastName, address:{city}} = person;

// console.log(city)

// console.log(person)
// ----------------------------------------------------------------
// loops 

// let todos = [
//     {
//         id: 1,
//         text: 'Take out trash',
//         isCompleted: true
//     },
//     {
//         id: 2,
//         text: 'Meeting with boss',
//         isCompleted: true
//     },
//     {
//         id: 3,
//         text: 'Dentist appt',
//         isCompleted: false
//     }
// ] 
// -----------------------------------------------------------
// for
// for(let i = 0; i < 10; i++) {
//     console.log(`For Loop Number: ${i}`);
// }

// for(let i = 0; i < todos.length; i++){
//     console.log(todos[i].text)
// }

// for(let todo of todos) {
//     console.log(todo.text)
// }
//  ----------------------------------------------------------
// forEach, map, filter 
// let todoText = todos.map(function(todo){
//     return todo.text
// })

// console.log(todoText)

// let todoCompleted = todos.filter(function(todo){
//     return todo.isCompleted === true
// })

// console.log(todoCompleted)
// -----------------------------------------------------------------
// contitions  if else
// let x = 20;

// if(x === 10){
//     console.log('x is 10')
// }
// else  if(x > 10){
//     console.log('x is greater than 10')
// }
// else {
//     console.log('x is less than 10')
// }
// /----------------------------------------------------------
// let x = 10

// let color = x > 10 ? 'red' : 'blue'

// switch(color) {
//     case 'red':
//         console.log('color is red')
//         break;
//     case 'blue':
//         console.log('color is blue')
//         break;
//     default:
//         console.log('color is Not red or blue')
// }
// ---------------------------------------------------
// functions

// function addNums(num1 = 1, num2 = 1) {
//     return num1 + num2;
// }
// console.log(addNums());
// ----------------------------------------------------------------

// function solution(input) {

// let firstNumber = Number(input.shift())
// let secondNumber = Number(input.shift())
// let thirdNumber = Number(input.shift())

// console.log(firstNumber + secondNumber + thirdNumber)

// }

// solution([
//     '2',
//     '3',
//     '5'
// ])
// ---------------------------------------------
// function solution(input) {

//     let firstName = input.shift()
//     let secondName = input.shift()
    
//     console.log(`Hello ${firstName} ${secondName}`)
    
//     }
//     solution([
//         'Velizar',
//         'Delyanov',
//     ])
