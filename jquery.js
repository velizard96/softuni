// SELECTORS
$(document).ready(function(){

// // $('h1').hide()
// // $('#heading1').hide()
// // $('.heading2').hide()
// $('p span').css('color', 'red')

// $('ul#list li:first').css('color', 'red')
// $('ul#list li:last').css('color', 'red')
// $('ul#list li:even').css('background-color', 'yellow')
// $('ul#list li:odd').css('background-color', 'gray')
// $('ul#list li:nth-child(3n)').css('list-style', 'none')

// // $(':button').hide()
// // $(':submit').hide()

// $('[href]').css('color', 'red')

// // $('*').hide()

// EVENTS

//     $('#btn1').click(function(){
//         alert('Button Clicked')
//     })
// // on method
//     $('#btn2').on('click', function(){
//         alert("Button Clicked")
//     })

// $('#btn1').click(function(){
//     $('.para1').hide()
//     // $('.para1').toggle()
// })

// $('#btn2').dblclick(function(){
//     $('.para1').show()
// })
//----------------------------------------------
// $('#btn1').on('mouseenter',function(){
//     $('.para1').toggle()
// })
// $('#btn1').on('mouseleave',function(){
//     $('.para1').toggle()
// })

// $('#btn1').on('mousemove',function(){
//         $('.para1').toggle()
//     })

// $('#btn1').on('mousedown',function(){
//         $('.para1').hide()
//     })

// $('#btn1').on('mouseup',function(){
//         $('.para1').show()
//     })
// -----------------------------------------------
// $('#btn1').click(function(e){
//            console.log(e.currentTarget.id)
//            console.log(e.currentTarget.innerHTML)
//            console.log(e.currentTarget.outerHTML)
//         })
// -----------------------------------------------

// $('input').focus(function(){
//     $(this).css('background-color', 'pink')
// })
// $('input').blur(function(){
//     $(this).css('background-color', 'white')
// })

// -----------------------------------------------
// $('input').keyup(function(e){
//     console.log(e.target.value)
// })

// $('select#gender').change(function(e){
//     alert(e.target.value)
// })
// -----------------------------------------------
// $('#form').submit(function(e){
//     e.preventDefault()
//     let name = $('input#name').val()
//     console.log(name)
// })
// DOM MANIPOLATION

// $('p.para1').css({
//     color: 'red',
//     background: 'gray'
// })
// // $('p.para2').addClass('customClass')
// // $('p.para2').removeClass('customClass')
// $('#btn').click(function(){
//     $('p.para2').toggleClass('customClass')
// })

// $('#myDiv').html('<h3> Hello World</h3>')
// alert($('#myDiv').text())

// ------------------------------------------------

// $('ul').append('<li>Append list item</li>')
// $('ul').prepend('<li>prepend list item</li>')
// $('ul').before('<h4>Hello</h4>')
// $('ul').after('<h4>Bye</h4>')
// // $('ul').empty()
// // $('ul').detach() - Delete element

// // -------------------------------------------------

// $('a').attr('target', 'blank')
// // $('a').remoeveAttr('target')

// ---------------------------------------------------

// $('p').wrap('<h1>')

// ----------------------------------------------------

// $('#newItem').keyup(function(e){
//     let code = e.which
//  if(code == 13){
//     e.preventDafault()
//     $('ul').append('<li>'+e.target.value+'</li>')
//  }  
// })

// -------------------------------------------------------

// let myArr = ['brad', 'nate', 'jose', 'rebeka']

// $.each(myArr, function(i, val){
//     $('#users').append('<li>'+val+'</li>')
// })

// let newArr = $('ul#list li').toArray()
// $.each(newArr, function(i, val){
//     console.log(val.innerHTML)

// ---------------Mini task--------------------------------

// $('.btn-red').click(function(){
//   $(this).parent().css('background', 'red')
//   $(this).parent().css('color', 'white')
// })

// $('.btn-green').click(function(){
//   $(this).parent().css('background', 'green')
//   $(this).parent().css('color', 'white')
// })

// $('.btn-blue').click(function(){
//   $(this).parent().css('background', 'blue')
//   $(this).parent().css('color', 'white')
// })

})